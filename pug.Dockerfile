FROM node:7.3.0

WORKDIR /app
RUN npm i -g pug-cli

VOLUME /app

CMD pug ./pug/ --pretty --watch --out ./public