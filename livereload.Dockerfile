FROM node:7.3.0

WORKDIR /deps
RUN npm install livereload

WORKDIR /deps/app
VOLUME /deps/app
EXPOSE 35729

CMD node livereload.js