FROM node:7.3.0

WORKDIR /app
RUN npm install -g less less-watch-compiler

VOLUME /app

CMD less-watch-compiler less public/build bundle.less